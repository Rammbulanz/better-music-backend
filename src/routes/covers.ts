import * as KoaRouter from 'koa-router';
import { CoverCollection } from '../types/CoverCollection';
import { SongManifest } from '../types/SongManifest';

const prefix = "/btm/cover";

const router = new KoaRouter({
    prefix: prefix
});
let covers: CoverCollection = {};

router.get('/', async ctx => {

   if (ctx.query.id && ctx.query.id in covers) {

      const cover = covers[ctx.query.id];

      ctx.type = cover.type;
      ctx.body = cover.data;

   }

});


export function addCover(manifest: SongManifest, type: string, data: Buffer) {

   covers[manifest.coverId] = {
      type,
      data
   };

   return prefix + "?id=" + manifest.coverId;
}

export default router;