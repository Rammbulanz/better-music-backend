import * as Router from 'koa-router'
import { createPlaylist } from '../common/playlists';

const router = new Router({
    prefix: "/btm/music"
});


router.post("/:profile/playlist/create/:name", async ctx => {

    try {

        const params: any = ctx.request.body || {};
        let includeingSongs = params.includingSongs || [];

        createPlaylist(ctx.params.profile, ctx.params.name, includeingSongs, true);


    } catch (err) {
        
        ctx.throw(500, err.stack || err);

    }

});



export default router;