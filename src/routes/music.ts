import * as Router from 'koa-router'
import * as music from '../common/music';
import { Song } from '../types/Song';
import { createReadStream } from 'fs';
import * as mime from 'mime';
import { AlbumResponse } from '../types/route-results/AlbumResponse';
import { SongResponse } from '../types/route-results/SongResponse';
import { ArtistResponse } from '../types/route-results/ArtistResponse';
import {albums, artists, songs} from "../common/music-iteration-helpers";

music.startLoadingLibrary();

const router = new Router({
    prefix: "/btm/music"
});

router.get("/library", async ctx => {
    ctx.type = "application/json";
    ctx.body = music.library;
});

router.get("/list/artists", async ctx => {
    ctx.type = "application/json";
    ctx.body = Array.from(artists());
});
router.get("/list/albums", async ctx => {
    ctx.type = "application/json";
    ctx.body = Array.from(albums(ctx.query.artist));
});
router.get("/list/songs", async ctx => {
    ctx.type = "application/json";
    ctx.body = Array.from(songs()).map(song => ({
        id: song.manifest.id,
            title: song.metadata.common.title,
        album: song.metadata.common.album,
        artist: song.metadata.common.albumartist || song.metadata.common.artist,
        meta: {
            duration: song.metadata.format.duration,
            type: "audio/" + song.metadata.format.dataformat
        }
    }));
});

router.get("/stream", async ctx => {

    if (ctx.query.id) {

        const song = music.songs[ctx.query.id] as Song;
        if (song) {
            ctx.type = mime.getType(song.file);
            ctx.body = createReadStream(song.file);
        } else {
            ctx.throw(404);
        }

    }

});

// Requests returning metadata info for a song
router.get("/library/:songId/interpret", async ctx => {
    // Read from database
});

router.get("/library/cover/album/:albumArtist/:albumTitle", async ctx => {

    try {

        // Go through songs of the album
        // Return the cover of the first song, that has a cover

        const songs = music.library[ctx.params.albumArtist][ctx.params.albumTitle];

        const songWithPicture = songs.find(s => s.metadata.common.picture && s.metadata.common.picture.length > 0);

        if (songWithPicture) {

            const picture = songWithPicture.metadata.common.picture[0];

            ctx.type = picture.format;
            ctx.body = picture.data;

        }

    } catch (err) {
        ctx.throw(err.stack);
    }

});
router.get("/library/cover/song/:songId", async ctx => {
    const song = music.songs[ctx.params.songId];

    if (!song) {
        return ctx.throw(404, `Song '${ctx.params.songId}'not found`);
    }

    if (song.metadata.common.picture.length > 0) {

        const picture = song.metadata.common.picture[0];

        ctx.type = picture.format;
        ctx.body = picture.data;

    } else {
        ctx.throw(404, `Song '${ctx.params.songId}' has no picture`);
    }

});

export default router;