import * as fs from 'fs'
import { join, resolve, dirname } from 'path';

const config = require(process.cwd() + "/config.json") as {
    /**
     * default path, where stuff like profiles and caches will be put
     */
    data: string

    server: {
        /**
         * Network port for Koa http server
         */
        port: number
    },

    database: {
        type: string
        host: string
        dbName: string
        collectionName: string
    },

    library: {
        /**
         * Path to folder where to scan library
         */
        path: string
        resolve: {
            /**
             * Array of string or reguular expressions, to test if a file is a music file
             */
            extension: Array<string | RegExp>
        }
    }
};

// Do some polyfills
// This should done synchronous
// May cause problems otherwise
// But this should be loaded once at application start
// So wayne
ensureDirectory(resolve(config.data));
ensureDirectory(resolve(config.data, "profiles"));

function ensureDirectory(dir: string) {

    if (!fs.existsSync(dir)) {

        const parent = dirname(dir);
        ensureDirectory(parent);

        fs.mkdirSync(dir);

    }

}
function ensureFile(file: string, defaultContent: string | Buffer, encoding: string) {

    if (!fs.existsSync(file)) {

        const parent = dirname(file);
        ensureDirectory(parent);

        fs.writeFileSync(file, defaultContent, encoding)

    }

}

export default config;