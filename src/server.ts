import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser'
import config from './config';
import musicRouter from './routes/music'
import playlistRouter from './routes/playlists'
import coverRouter from './routes/covers'

const app = new Koa();

// Use bodyParser for post requests
app.use(bodyParser());

// logger

app.use(async (ctx, next) => {
   await next();
   const rt = ctx.response.get('X-Response-Time');
   console.log(`${ctx.method} ${ctx.url} - ${rt}`);
 });
 
 // x-response-time
 
 app.use(async (ctx, next) => {
   const start = Date.now();
   await next();
   const ms = Date.now() - start;
   ctx.set('X-Response-Time', `${ms}ms`);
 });
 
 // response
app.use(musicRouter.routes()).use(musicRouter.allowedMethods());
app.use(coverRouter.routes()).use(coverRouter.allowedMethods())
app.use(playlistRouter.routes()).use(playlistRouter.allowedMethods());
 
 app.listen(3000);
 console.log("Server started. Listening on port " + config.server.port);