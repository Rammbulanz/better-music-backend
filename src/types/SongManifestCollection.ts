import { SongManifest } from "./SongManifest";

export type SongManifestCollection = {
    [file: string]: SongManifest
}