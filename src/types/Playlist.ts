import { Song } from "./Song";

export type Playlist = {
    name: string
    songs: Array<Song>
}