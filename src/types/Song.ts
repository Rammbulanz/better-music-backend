import { ICommonTagsResult, IAudioMetadata } from 'music-metadata';
import { SongManifest } from './SongManifest';

export type Song = {
   file: string
   metadata: IAudioMetadata
   manifest: SongManifest
}