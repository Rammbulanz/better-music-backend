import { Song } from "./Song";

export type MusicCollection = {
   [artist: string]: {
      [album: string]: Array<Song>
   }
}