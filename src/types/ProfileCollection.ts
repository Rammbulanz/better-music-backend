import { Profile } from "./Profile";

export type ProfileCollection = {
   [profileName: string]: Profile
}