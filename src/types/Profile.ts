import { MusicCollection } from "./MusicCollection";

export type Profile = {
   name: string
   music: MusicCollection
};