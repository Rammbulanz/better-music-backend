/**
 * Used to store some data about the song file, that may used to remember stuff along sessions.
 */
export type SongManifest = {
    /**
     * Universal id for the song
     */
    id: string
    /**
     * ID for the cover route request
     */
    coverId: string
}