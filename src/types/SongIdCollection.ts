import { Song } from "./Song";

export type SongIdCollection = {
   [id: string]: Song
}