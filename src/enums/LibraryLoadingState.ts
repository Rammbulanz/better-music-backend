export enum LibraryLoadingState {
   NotStarted = "NotStarted",
   Loading = "LOADING",
   Loaded = "LOADED",
   Failed = "FAILED"
}