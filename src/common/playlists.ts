/**
 * @todo Caching
 */

import { getProfileDirectory, getProfile, createProfile } from "./profiles";
import { Song } from "../types/Song";
import { resolve } from "path";
import * as afs from '@rammbulanz/afs'
import * as glob from 'glob'
import { Profile } from "../types/Profile";
import { Playlist } from "../types/Playlist";

export async function createPlaylist(profileName: string, name: string, includingSongs: Array<Song>, createIfNotExists = false) {

    const profile = await getProfile(profileName);

    if (!profile) {
        if (createIfNotExists) {
            await createProfile(profileName, "", true);
        } else {
            throw new Error(`Profile ${profileName} doesn't exist.`);
        }
    }

    const directoryProfile = getProfileDirectory(profile);
    const directoryPlaylists = getPlaylistsDirectory(profile);
    const filePlaylist = resolve(directoryPlaylists, name + ".json");

    await afs.mkdirs(directoryPlaylists);

    if (await afs.exists(filePlaylist)) {
        throw new Error(`Playlist ${name} does already exist.`);
    }

    await afs.writeFile(filePlaylist, JSON.stringify({
        name,
        songs: includingSongs
    }), "utf8");

}

/**
 * Look for playlist files. Check each file, if it looks like a playlist.
 * - Pattern: `${config.data}/${profileName}/playlists/*.json`
 * @param profileName 
 */
export async function listPlaylists(profileName: string) {

    const profile = await getProfile(profileName);

    if (!profile) {
        throw new Error(`Profile ${profileName} does'nt exist`)
    }

    // List the files

    const directory_playlists = getPlaylistsDirectory(profileName);
    let playlist_files: string[];

    await new Promise((fulfill, reject) => {
        const pattern = resolve(getProfileDirectory(profileName), "*.json");
        glob(pattern, (err, matches) => {
            if (err) {
                return reject(err);
            }
            fulfill(matches);
        });
    });

    if (playlist_files) {

        // find valid files

        const playlist_fetching_tasks = playlist_files.map(file => new Promise(async (fulfill, reject) => {

            try {

                const stat = await afs.stat(file);
                if (!stat.isFile()) {
                    throw "Not a file";
                }

                const content = await afs.readFile(file, "utf8");
                const playlist = JSON.parse(content as string) as Playlist;

                if (!isValidPlaylistObject(playlist)) {
                    throw "Failed validation check";
                }

                fulfill(playlist);

            } catch (err) {
                reject(err);
            }

        }));

        // Catch all tasks, to prevent node from crying
        playlist_fetching_tasks.forEach(task => task.catch(function () {}));

        return await Promise.all(playlist_fetching_tasks);
    }

    return [];
}

/**
 * Validate a playlist object
 * @param object 
 */
export function isValidPlaylistObject(object: Playlist) {

    if (typeof object !== "object") {
        return false;
    }

    if (typeof object.name !== "string") {
        return false;
    }

    if (!(object.songs instanceof Array)) {
        return false;
    }

    return true;
}

export function getPlaylistsDirectory(profile: Profile | string) {
    return resolve(getProfileDirectory(profile), "playlists")
}