import * as mongo from 'mongodb'
import config from '../config';

let client: mongo.MongoClient;
let database: mongo.Db;
let collection: mongo.Collection


export interface Document {
    _id: string
}


export async function readDocument<T = any>(documentId: string): Promise<T> {

    if (!client)
        await connect();

    if (!collection) {
        throw new Error("Missing `collection`");
    }

    const findResult = await collection.find<T>({ _id: documentId }).toArray();
    let result: T;

    if (findResult.length > 0) {
        result = findResult[0];
    } else {
        throw new Error("Not found")
    }

    return result;
}

export async function writeDocument<T extends Document>(documentId: string, content: T) {

    if (!client)
        await connect();

    content._id = documentId;

    collection.insertOne(content, (result) => {
        result.code
        debugger;
    });

}

export async function connect() {
    if (!client || !client.isConnected()) {
        client = await mongo.connect(`mongodb://${config.database.host}`);
        database = client.db(config.database.dbName);
        collection = database.collection(config.database.collectionName);
    }
}
export async function disconnect() {
    if (client) {
        client.close();
    }
}