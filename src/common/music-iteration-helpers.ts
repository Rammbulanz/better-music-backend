import {library} from "./music";
import {ArtistResponse} from "../types/route-results/ArtistResponse";
import {AlbumResponse} from "../types/route-results/AlbumResponse";

export function* songs() {
    for (let artist in library) {
        for (let album in library[artist]) {
            for (let song of library[artist][album])
                yield song;
        }
    }
}

export function* artists() {
    for (let artist in library) {
        yield {
            name: artist
        } as ArtistResponse;
    }
}

export function* albums(...artistFilter: string[]) {
    for (let artist of artists()) {

        if (artistFilter && artistFilter.indexOf(artist.name) != -1) {
            continue;
        }

        for (let album in library[artist.name]) {

            yield {
                artist: artist.name,
                songs: library[artist.name][album].map(s => s.manifest.id),
                title: album
            } as AlbumResponse;

        }
    }
}