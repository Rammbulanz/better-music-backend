import { Profile } from '../types/Profile'
import config from "../config";
import { resolve } from "path";
import * as afs from '@rammbulanz/afs'
import * as music from './music';
import {ProfileCollection} from "../types/ProfileCollection";
import {library} from "./music";

export let profiles: ProfileCollection = {};

/**
 * Create a new Profile
 * @param name The name for the profile
 * @param password The password for the profile (Not implemented)
 * @param importAllMusic (Not implemented)
 */
export async function createProfile(name: string, password: string, importAllMusic = true) {
    const profile: Profile = {
        name: name,
        music: {}
    };

    if (importAllMusic) {
        await music.task;
        profile.music = library;
    }

    // Create folder
    const profileDir = getProfileDirectory(profile);
    await afs.mkdirs(profileDir);

    // Create file
    await afs.writeFile(resolve(profileDir, "profile.json"), JSON.stringify(profile, undefined, 2), "utf8");

    // Create credentials file
    await afs.writeFile(resolve(profileDir, "credentials"), JSON.stringify({ login: profile.name, password }, undefined, 2), "utf8");

    return profile;

}

export async function deleteProfile(profile: Profile) {
    if (await profileExists(profile.name)) {
        await afs.rmdir(getProfileDirectory(profile), true);
    }
}

/**
 * Check wheather the pfoile exists or not
 * - Check if `${config.data}/${name}/profile.json` exists.
 * @param name 
 */
export async function profileExists(name: string) {
    const profileFile = resolve(getProfileDirectory(name), "profile.json");
    return await afs.exists(profileFile)
}

/**
 * If the profile exists, read its file and return it.
 * If the profile doesn't exist, return `null`
 * @param name The name of the profile
 */
export async function getProfile(name: string) {

    if (await profileExists(name)) {

        const dir = getProfileDirectory(name);
        const file = resolve(dir, "profile.json");

        return JSON.parse(await afs.readFile(file, "utf8") as string) as Profile;

    }

    return null;
}

export function getProfileDirectory(profile: Profile | string) {
    return resolve(config.data, "profiles", (profile as Profile).name || name);
}