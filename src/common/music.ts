import {MusicCollection} from "../types/MusicCollection";
import {LibraryLoadingState} from "../enums/LibraryLoadingState";
import * as afs from '@rammbulanz/afs'
import config from "../config";
import * as mm from 'music-metadata'
import {SongManifest} from "../types/SongManifest";
import * as uuid from 'uuid'
import {SongManifestCollection} from "../types/SongManifestCollection";
import {Song} from "../types/Song";
import {SongIdCollection} from "../types/SongIdCollection";

export let library: MusicCollection = {};
export let manifests: SongManifestCollection = {};
export let songs: SongIdCollection = {};

export let task: Promise<MusicCollection>;
export let status = LibraryLoadingState.NotStarted;

/**
 * Start loading the library.
 * If a task is already running, return that task.
 * If no task is running, create a new task and return it.
 */
export function startLoadingLibrary() {

    if (task) {
        return task;
    }

    console.log("Loading library..")
    task = _loadLibrary();

    task.then(() => {
        status = LibraryLoadingState.Loaded;
        console.log("Library loaded")
    });
    task.catch((err) => {
        status = LibraryLoadingState.Failed;
        if (err) {
            console.error("Failed loading library: " + (err.stack || err));
        }
    });

    return task;
}

/**
 *
 * @private
 */
async function _loadLibrary() {
    await _openFolder(config.library.path);
    return library
}

/**
 *
 * @param folder
 * @private
 */
async function _openFolder(folder: string) {

    try {

        // Print error and return, if folder doesn't exist
        if (!(await afs.exists(folder))) {
            console.error(`Folder '${folder}' doesn't exist`);
            return;
        }

        // Now scan the folder

        const content = await afs.readdir(folder);
        for (let item of content) {


            const stats = await afs.stat(item);

            if (stats) {

                if (stats.isFile()) {
                    await _openFile(item);
                } else if (stats.isDirectory()) {
                    await _openFolder(item)
                } else if (stats.isSymbolicLink()) {
                    console.warn("Symbolic links not supported for now");
                }

            }

        }

    } catch (err) {
        console.error(err.stack || err);
    }

}

function _isMusicFile(file: string) {

    for (let resolvePattern of config.library.resolve.extension) {

        let regex = new RegExp(resolvePattern);
        if (regex.test(file)) {
            return true;
        }

    }

    return false;
}

async function _openFile(file: string) {

    try {

        const isMusicFile = _isMusicFile(file);
        if (!isMusicFile) {
            return;
        }

        const metadata = await mm.parseFile(file);
        const manifest = await _getManifest(file, metadata);

        addSong(file, metadata, manifest);

    } catch (err) {
        console.error(err.stack || err);
    }

}

export function addSong(file: string, metadata: mm.IAudioMetadata, manifest: SongManifest) {

    const song: Song = {
        file: file,
        metadata: metadata,
        manifest: manifest
    };

    manifests[file] = manifest;

    const artist = metadata.common.artist;
    const album = metadata.common.album;

    if (!(artist in library)) {
        library[artist] = {};
    }
    if (!(album in library[artist])) {
        library[artist][album] = [];
    }

    library[artist][album].push(song);
    songs[manifest.id] = song;

}

async function _getManifest(file: string, metadata: mm.IAudioMetadata): Promise<SongManifest> {

    // First check, if we already loaded the manifest for the file

    if (manifests[file]) {
        return manifests[file];
    }

    // Then check if there is a manifest file for the song file

    const manifestFile = file + ".manifest";

    if (await afs.exists(manifestFile)) {
        return JSON.parse(await afs.readFile(manifestFile, "utf8") as string) as SongManifest
    }

    // No manifest defined. Create new one.

    return {
        id: uuid(),
        coverId: uuid()
    };
}